/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tijo.pkg05;

/**
 *
 * @author student
 */
public class FeedMessage {
    String title;
    String description;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description.replaceAll("\\<[^>]*>","").replaceAll("&quot;", "'");
        
    }

    @Override
    public String toString() {
        String txt="Tytuł: " + title + "\nOpis: " + description + "\n\n";
                StringBuilder sb = new StringBuilder(txt);
            int i = 0;
            while ((i = sb.indexOf(" ", i + 90)) != -1) {
                sb.replace(i, i + 1, "\n");
            }
            
        return sb.toString();
    }
}
